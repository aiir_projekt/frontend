FROM nginx:alpine
COPY dist/aiir-frontend/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
