export const environment = {
  production: true,
  apiUrl: 'http://localhost:8000/',
  wsEndpoint: 'ws://localhost:8000/fractal_generator/ws',
};
