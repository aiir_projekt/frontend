import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { UserDetailsService } from 'src/app/services/user-details.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent implements OnInit, OnDestroy {
  userSubscription = new Subscription();
  user$: User;

  constructor(private userDetails: UserDetailsService) {}

  ngOnInit(): void {
    this.userSubscription = this.userDetails.userDetails.subscribe((user) => {
      this.user$ = user;
    });
    this.userDetails.getUserDetails();
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
