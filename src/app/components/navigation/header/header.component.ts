import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  isUser: Observable<User>;

  constructor(private auth: AuthenticationService, private router: Router) {}

  ngOnInit(): void {
    this.isUser = this.auth.currentUser;
  }

  navProfile(): any {
    this.router.navigate(['/profile']);
  }

  navLogin(): any {
    this.router.navigate(['/login']);
  }

  logout(): any {
    this.auth.logout();
    this.navLogin();
  }
}
