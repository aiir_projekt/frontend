import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
})
export class LandingComponent implements OnInit {
  constructor(private auth: AuthenticationService, private router: Router) {
    if (this.auth.currentUserValue) {
      this.router.navigate(['/fractals']);
    }
  }

  ngOnInit(): void {}
}
