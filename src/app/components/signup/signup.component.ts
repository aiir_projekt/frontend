import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { first } from 'rxjs/operators';
import { NotificationService } from 'src/app/services/notification.service';
import { SignupService } from 'src/app/services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  loading: boolean = false;

  get emailControl(): AbstractControl {
    return this.signupForm.controls.email;
  }

  get passwordControl(): AbstractControl {
    return this.signupForm.controls.password;
  }

  get firstNameControl(): AbstractControl {
    return this.signupForm.controls.firstName;
  }

  get lastNameControl(): AbstractControl {
    return this.signupForm.controls.lastName;
  }

  constructor(
    private formBuilder: FormBuilder,
    private signupService: SignupService,
    private readonly notificationsService: NotificationService
  ) {}

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
    });
  }

  onSubmit() {
    this.loading = true;
    this.signupService
      .signup(
        this.emailControl.value,
        this.passwordControl.value,
        this.firstNameControl.value + this.lastNameControl.value
      )
      .pipe(first())
      .subscribe({
        next: () => {
          this.loading = false;
          this.signupForm.reset();
          this.notificationsService.showSuccess('Konto utworzone. Możesz się teraz zalogować');
        },
        error: (error) => {
          this.loading = false;
          this.notificationsService.showError(error);
        },
      });
  }
}
