import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ImageService } from 'src/app/services/image.service';

@Component({
  selector: 'app-fractals',
  templateUrl: './fractals.component.html',
  styleUrls: ['./fractals.component.css'],
})
export class FractalsComponent implements OnInit, OnDestroy {
  fractalForm: FormGroup;
  image: string;
  imageSub$: Subscription;

  @ViewChild('imageDiv')
  imageDivIdentifier: ElementRef;

  getControl(key: string | number) {
    return this.fractalForm.controls[key];
  }

  modifyControl(key: string | number, action: string, value: number) {
    switch (action) {
      case '-':
        this.getControl(key).setValue(this.getControl(key).value - value);
        break;
      case '+':
        this.getControl(key).setValue(this.getControl(key).value + value);
        break;
      case '/':
        this.getControl(key).setValue(this.getControl(key).value / value);
        break;
      case '*':
        this.getControl(key).setValue(this.getControl(key).value * value);
        break;
    }
    this.getControl(key).setValue(this.roundTo(this.getControl(key).value, 2));
  }

  private roundTo(num: number, places: number){
    const factor = 10 ** places;
    return Math.round(num * factor) / factor;
  }

  setImage(image: string) {
    setTimeout(() => {
      this.image = image;
    }, 10);
  }

  constructor(
    private formBuilder: FormBuilder,
    private imageService: ImageService
  ) {}

  ngOnInit(): void {
    this.fractalForm = this.formBuilder.group({
      type: ['', Validators.required],
      iters: [200, Validators.required],
      min_x: [-1, Validators.required],
      max_x: [1, Validators.required],
      min_y: [-1, Validators.required],
      max_y: [1, Validators.required],
      w: [100, Validators.required],
      h: [100, Validators.required],
    });
    this.imageService.connect();
    this.imageSub$ = this.imageService.image$
      .asObservable()
      .subscribe((data) => this.setImage(data));
  }

  ngOnDestroy(): void {
    this.imageService.disconnect();
    this.imageSub$.unsubscribe();
  }

  onSubmit() {
    this.sendRequest(0);
  }

  private enabled = false;
  sendRequest(timeout: number) {
    if (this.enabled == false) {
      if (this.fractalForm.valid) {
        this.enabled = true;
        this.setImageHeight();
        this.setImageWidth();
        setTimeout(() => {
          this.imageService.send(this.fractalForm.value);
          this.enabled = false;
        }, timeout);
      }
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    switch (event.key) {
      case 'ArrowDown':
        const diff1 =
          (this.getControl('max_y').value - this.getControl('min_y').value) *
          0.1;
        this.modifyControl('max_y', '-', diff1);
        this.modifyControl('min_y', '-', diff1);
        this.sendRequest(750);
        break;
      case 'ArrowUp':
        const diff2 =
          (this.getControl('max_y').value - this.getControl('min_y').value) *
          0.1;
        this.modifyControl('max_y', '+', diff2);
        this.modifyControl('min_y', '+', diff2);
        this.sendRequest(750);
        break;
      case 'ArrowLeft':
        const diff4 =
          (this.getControl('max_x').value - this.getControl('min_x').value) *
          0.1;
        this.modifyControl('max_x', '-', diff4);
        this.modifyControl('min_x', '-', diff4);
        this.sendRequest(750);
        break;
      case 'ArrowRight':
        const diff3 =
          (this.getControl('max_x').value - this.getControl('min_x').value) *
          0.1;
        this.modifyControl('max_x', '+', diff3);
        this.modifyControl('min_x', '+', diff3);
        this.sendRequest(750);
        break;
      case '+':
        this.modifyControl('max_x', '/', 1.2);
        this.modifyControl('min_x', '/', 1.2);
        this.modifyControl('max_y', '/', 1.2);
        this.modifyControl('min_y', '/', 1.2);
        this.sendRequest(750);
        break;
      case '=':
        this.modifyControl('max_x', '/', 1.2);
        this.modifyControl('min_x', '/', 1.2);
        this.modifyControl('max_y', '/', 1.2);
        this.modifyControl('min_y', '/', 1.2);
        this.sendRequest(750);
        break;
      case '-':
        this.modifyControl('max_x', '*', 1.2);
        this.modifyControl('min_x', '*', 1.2);
        this.modifyControl('max_y', '*', 1.2);
        this.modifyControl('min_y', '*', 1.2);
        this.sendRequest(750);
        break;
      case 'Enter':
        this.sendRequest(0);
        break;
    }
  }

  setImageWidth() {
    this.getControl('w').setValue(
      this.imageDivIdentifier.nativeElement.offsetWidth
    );
  }

  setImageHeight() {
    this.getControl('h').setValue(
      this.imageDivIdentifier.nativeElement.offsetHeight
    );
  }
}
