import { Inject, Injectable } from '@angular/core';
import { TuiNotification, TuiNotificationsService } from '@taiga-ui/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(@Inject(TuiNotificationsService)
  private readonly notificationsService: TuiNotificationsService) { }

  showInfo(message){
    this.notificationsService
      .show(message, {
        label: 'Informacja',
        status: TuiNotification.Info,
      })
      .subscribe();
  }

  showSuccess(message) {
    this.notificationsService
      .show(message, {
        label: 'Sukces',
        status: TuiNotification.Success,
      })
      .subscribe();
  }

  showWarning(message){
    this.notificationsService
      .show(message, {
        label: 'Ostrzeżenie',
        status: TuiNotification.Warning,
      })
      .subscribe();
  }

  showError(message){
    this.notificationsService
      .show(message, {
        label: 'Błąd',
        status: TuiNotification.Error,
      })
      .subscribe();
  }
}
