import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class UserDetailsService {
  userDetails = new Subject<User>();
  user: User;

  constructor(private http: HttpClient, private auth: AuthenticationService) {}

  getUserDetails() {
    if (!this.auth.isAuthenticated()) {
      return;
    }
    return this.http
      .get<any>(`${environment.apiUrl}user/me`)
      .pipe(
        retry(3),
        map((data) => {
          return data.data[0];
        })
      )
      .subscribe((data: User) => {
        this.user = data;
        this.userDetails.next({ ...this.user });
      });
  }
}
