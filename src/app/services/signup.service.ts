import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SignupService {
  constructor(private http: HttpClient) {}

  signup(email: string, password: string, fullName: string) {
    return this.http.post<any>(`${environment.apiUrl}user/register`, {
      email: email,
      password: password,
      fullname: fullName,
    });
  }
}
