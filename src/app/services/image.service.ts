import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { retry } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from './authentication.service';
import { NotificationService } from './notification.service';

@Injectable({ providedIn: 'root' })
export class ImageService {
  image$ = new Subject<string>();

  private readonly WS_ENDPOINT = environment.wsEndpoint;
  private websocket: WebSocketSubject<any>;

  constructor(private notificationService: NotificationService, private auth: AuthenticationService) {}

  connect() {
    this.websocket = webSocket(this.WS_ENDPOINT);
    this.websocket.subscribe(
      msg => {
        this.image$.next(JSON.parse(msg).image);
      },
      err => {
        this.notificationService.showError(err.message);
        retry(3)
      },
      () => {
        this.notificationService.showInfo('Połączenie zakończone');
      }
    )
  }

  disconnect() {
    this.websocket.complete();
  }

  send(message: any) {
    this.websocket.next({
      ...message,
      id: this.auth.currentUserValue.sub
    });
  }

}
