import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TuiRootModule } from '@taiga-ui/core';
import {
  iconsPathFactory,
  TUI_ICONS_PATH,
  TuiNotificationsModule,
} from '@taiga-ui/core';
import { TuiAccordionModule } from '@taiga-ui/kit';
import { TuiInputModule } from '@taiga-ui/kit';
import { TuiInputPasswordModule } from '@taiga-ui/kit';
import { TuiButtonModule } from '@taiga-ui/core';
import { TuiFieldErrorModule } from '@taiga-ui/kit';
import { TuiThemeNightModule, TuiModeModule } from '@taiga-ui/core';
import { TuiIslandModule } from '@taiga-ui/kit';
import { TuiRadioModule } from '@taiga-ui/kit';
import { TuiRadioBlockModule } from '@taiga-ui/kit';
import { TuiInputCountModule } from '@taiga-ui/kit';

const modules = [
  TuiRootModule,
  TuiAccordionModule,
  TuiInputModule,
  TuiInputPasswordModule,
  TuiButtonModule,
  TuiFieldErrorModule,
  TuiThemeNightModule,
  TuiModeModule,
  TuiNotificationsModule,
  TuiIslandModule,
  TuiRadioModule,
  TuiRadioBlockModule,
  TuiInputCountModule,
];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...modules],
  exports: [...modules],
  providers: [
    {
      provide: TUI_ICONS_PATH,
      useValue: iconsPathFactory('assets/taiga-ui/icons/'),
    },
  ],
})
export class TaigaModule {}
